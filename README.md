# osc

[![Documentation](http://godoc.org/github.com/goosc/osc?status.png)](http://godoc.org/github.com/goosc/osc)

## Installation

It is recommended to use Go 1.11 with module support (`$GO111MODULE=on`).

```
go get -d github.com/goosc/osc/...
```


## Example

```go
package main

import (
	"fmt"

	"github.com/goosc/osc"
)

const (
	click     osc.Path = "/click"
	play      osc.Path = "/play"
	trackSolo osc.Path = "/track/%d/solo"
)

type receiver struct{}

func (r receiver) Matches(p osc.Path) bool { return p != "/time/str" }

func (r receiver) Handle(p osc.Path, values ...interface{}) {
	fmt.Printf("got: %#v (%v)\n", p, values)
	if p.HasPrefix("/track/") {
		var track int
		p.Scan("/track/%d", &track)
		fmt.Printf("track is %d\n", track)
	}
}

func main() {
	var _c chan bool
	listener, err := osc.UDPListener("127.0.0.1:9000")
	errPanic(err)

	defer listener.StopListening()

	go listener.StartListening(receiver{})

	wr, err := osc.UDPWriter("127.0.0.1:8000")
	errPanic(err)
	defer wr.Close()

	click.WriteTo(wr, 1)
	play.WriteTo(wr, 1)
	trackSolo.Set(1).WriteTo(wr, 1)
	<-_c
}

func errPanic(err error) {
	if err != nil {
		panic(err.Error())
	}
}

```

### Status 

WIP