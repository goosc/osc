package osc

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io"
)

// bundleTag is the tag on an OSC bundle message.
const bundleTag = "#bundle"
const timetagSize = 8

func _parse(data []byte, d *dispatcherr) error {
	switch data[0] {
	case '#':
		//bundle, err := ParseBundle(data, incoming.Sender)
		bundle, err := parseBundle(data)
		if err != nil {
			return err
		}
		if err := d.dispatchh(bundle); err != nil {
			return err
		}
	case '/':
		//msg, err := ParseMessage(data, incoming.Sender)
		msg, err := _parseMessage(data)
		if err != nil {
			return err
		}
		if err := d.invoke2(msg); err != nil {
			return err
		}
	}
	return nil
}

// SecondsFrom1900To1970 is exactly what it sounds like.
const secondsFrom1900To1970 = 2208988800

// NewBundle returns a new bundle.
func NewBundle(tt Timetag, packets ...io.WriterTo) Bundle {
	return Bundle{timetag: tt, packets: packets}
}

// Bundle is an OSC bundle.
// An OSC Bundle consists of the OSC-string "#bundle" followed by an OSC Time Tag,
// followed by zero or more bundle elements. The OSC-timetag is a 64-bit fixed
// point time tag. See http://opensoundcontrol.org/spec-1_0 for more information.
type Bundle struct {
	timetag Timetag
	packets []io.WriterTo //[]Packet
	//	Sender  net.Addr
}

// ParseBundle parses a bundle from a byte slice.
//func ParseBundle(data []byte, sender net.Addr) (Bundle, error) {
func parseBundle(data []byte) (Bundle, error) {
	//	return parseBundle(data, sender, -1)
	return _parseBundle(data, -1)
}

// parseBundle parses a bundle from a byte slice.
// It will stop after reading limit bytes.
// If you wish to have it consume as many bytes as possible, pass -1 as the limit.
//func parseBundle(data []byte, sender net.Addr, limit int32) (Bundle, error) {
func _parseBundle(data []byte, limit int32) (Bundle, error) {
	b := Bundle{}

	// If 0 <= limit < 16 this is an error.
	// We have to be able to read at least the bundle tag and a timetag.
	if (limit >= 0) && (limit < int32(len(bundleTag)+1+timetagSize)) {
		return b, fmt.Errorf("limit must be >= 16 or < 0")
	}

	data, err := sliceBundleTag(data)
	if err != nil {
		return b, fmt.Errorf("slice bundle tag: %v", err)
	}

	tt, err := readTimetag(data)
	if err != nil {
		return b, fmt.Errorf("read timetag: %v", err)
	}
	b.timetag = tt
	data = data[8:]

	// We are limited to only reading the bundle tag and the timetag.
	if limit == 16 {
		return b, nil
	}

	// We take away 16 from limit so that readPackets doesn't have to know we have already read 16 bytes.
	//packets, err := readPackets(data, sender, limit-16)
	packets, err := readPackets(data, limit-16)
	if err != nil {
		return b, fmt.Errorf("read packets: %v", err)
	}
	b.packets = packets

	return b, nil
}

// WriteTo writes the bundle to an io.Writer.
func (b Bundle) WriteTo(w io.Writer) (int64, error) {
	n, err := w.Write(b.bytes())
	return int64(n), err
}

// Bytes returns the contents of the bundle as a slice of bytes.
func (b Bundle) bytes() []byte {
	bss := [][]byte{
		stringToBytes(bundleTag),
		b.timetag.bytes(),
	}
	for _, p := range b.packets {
		var bf bytes.Buffer
		p.WriteTo(&bf)
		var (
			bs     = bf.Bytes() //p.bytes()
			length = int32(len(bs))
		)
		bss = append(bss, intToBytes(length), bs)
	}
	return bytes.Join(bss, []byte{})
}

// sliceBundleTag slices the bundle tag off the data.
// If the bundle tag is not present or is not correct, an error is returned.
func sliceBundleTag(data []byte) ([]byte, error) {
	_bundleTag := append([]byte(bundleTag), 0)
	if len(data) < len(bundleTag) {
		return nil, fmt.Errorf("expected %q, got %q", _bundleTag, data)
	}
	idx := bytes.Index(data, _bundleTag)
	if idx == -1 {
		return nil, fmt.Errorf("expected %q, got %q", _bundleTag, data[:len(_bundleTag)])
	}
	return data[len(_bundleTag):], nil
}

var errEndOfPackets = fmt.Errorf("end of packets")

// readPackets reads bundle packets from a byte slice.
//func readPackets(data []byte, sender net.Addr, limit int32) ([]Packet, error) {
func readPackets(data []byte, limit int32) ([]io.WriterTo, error) {
	ps := []io.WriterTo{}

	var (
		p   io.WriterTo
		l   int32
		err error
	)
	for {
		//p, l, err = readPacket(data, sender)
		p, l, err = readPacket(data)
		if err == errEndOfPackets {
			return ps, nil
		}
		if err != nil {
			return nil, fmt.Errorf("read packet: %v", err)
		}
		ps = append(ps, p)
		if l+4 == int32(len(data)) {
			break
		}
		if (limit >= 0) && (limit <= l+4) {
			break
		}
		data = data[l+4:]
	}
	return ps, nil
}

// readPacket reads an OSC bundle packet from a byte slice.
// The packet and the packet length are returned along with nil if there was no error.
// If the packet length is 0 then ErrEndOfPackets is returned as the error.
// If ErrEndOfPackets is returned then Packet will always be nil.
// The returned packet length includes the length of the packet length integer itself,
// so it is actually packet_length + 4.
//func readPacket(data []byte, sender net.Addr) (Packet, int32, error) {
func readPacket(data []byte) (io.WriterTo, int32, error) {
	if len(data) < 4 {
		return nil, int32(len(data)), errEndOfPackets
	}
	var l int32
	_ = binary.Read(bytes.NewReader(data), byteOrder, &l) // Never fails
	if l == int32(0) {
		return nil, 0, errEndOfPackets
	}

	data = data[4:]

	if int32(len(data)) < l {
		return nil, 0, fmt.Errorf("packet length %d is greater than data length %d", l, len(data))
	}

	switch data[0] {
	case '/':
		msg, err := _parseMessage(data)
		if err != nil {
			return nil, 0, fmt.Errorf("parse message from packet: %v", err)
		}
		return msg, l, nil // The returned length includes the packet length integer.
	case '#':
		bundle, err := _parseBundle(data, l)
		if err != nil {
			return nil, 0, fmt.Errorf("parse bundle from packet: %v", err)
		}
		return bundle, l, nil // The returned length includes the packet length integer.
	default:
		return nil, 0, fmt.Errorf("packet should never start with %c", data[0])
	}
}
