package osc

import (
	"context"
	"fmt"
	"io"
	"net"
	"strings"
)

const bufSize = 65536

func UDPListener(address string) (Listener, error) {
	laddr2, err := net.ResolveUDPAddr("udp", address)
	if err != nil {
		return nil, fmt.Errorf("can't listen on udp %s: %s", address, err.Error())
	}
	return listenUDP("udp", laddr2)

}

func UDPWriter(address string) (io.WriteCloser, error) {
	raddr2, err := net.ResolveUDPAddr("udp", address)
	if err != nil {
		return nil, fmt.Errorf("can't send on udp %s: %s", address, err.Error())
	}
	return dialUDP("udp", nil, raddr2)

}

type netWriter interface {
	SetWriteBuffer(bytes int) error
	WriteTo([]byte, net.Addr) (int, error)
}

// readSender knows how to read bytes and return the net.Addr
// of the sender of the bytes.
type readSender interface {
	CloseChan() <-chan struct{}
	Context() context.Context
	read([]byte) (int, net.Addr, error)
}

func workerLoop(r readSender, d *dispatcherr) {
	for {
		data := make([]byte, bufSize)
		_, _, err := r.read(data)
		if err != nil {
			// Tried non-blocking select on closeChan right before ReadFromUDP
			// but that didn't stop us from reading a closed connection. [briansorahan]
			if strings.Contains(err.Error(), "use of closed network connection") {
				return
			}
			//errChan <- err
			return
		}

		// Get the next worker.
		//worker := <-ready

		// Assign them the data we just read.
		//worker.DataChan <- Incoming{Data: data, Sender: sender}
		_parse(data, d)
	}
}

func serve(r readSender, dispatcher *dispatcherr) error {
	go workerLoop(r, dispatcher)
	//Parse(data []byte, d *Dispatcher) error
	/*
		if err := checkDispatcher(dispatcher); err != nil {
			return err
		}
		var (
			errChan = make(chan error)
			ready   = make(chan Worker, numWorkers)
		)
		for i := 0; i < numWorkers; i++ {
			go Worker{
				DataChan:   make(chan Incoming),
				Dispatcher: dispatcher,
				ErrChan:    errChan,
				Ready:      ready,
				ExactMatch: exactMatch,
			}.Run()
		}
		go workerLoop(r, ready, errChan)

		// If the connection is closed or the context is canceled then stop serving.
		select {
		case err := <-errChan:
			return errors.Wrap(err, "error serving udp")
		case <-r.CloseChan():
		case <-r.Context().Done():
			return r.Context().Err()
		}
	*/
	return nil
}

// udpConn includes exactly the methods we need from *net.UDPConn
type udpConn interface {
	net.Conn
	netWriter

	ReadFromUDP([]byte) (int, *net.UDPAddr, error)
}

// UDPConn is an OSC connection over UDP.
type uDPConn struct {
	udpConn

	closeChan chan struct{}
	ctx       context.Context
	errChan   chan error
}

// DialUDP creates a new OSC connection over UDP.
func dialUDP(network string, laddr, raddr *net.UDPAddr) (*uDPConn, error) {
	return dialUDPContext(context.Background(), network, laddr, raddr)
}

// DialUDPContext returns a new OSC connection over UDP that can be canceled with the provided context.
func dialUDPContext(ctx context.Context, network string, laddr, raddr *net.UDPAddr) (*uDPConn, error) {
	conn, err := net.DialUDP(network, laddr, raddr)
	if err != nil {
		return nil, err
	}
	uc := &uDPConn{
		udpConn:   conn,
		closeChan: make(chan struct{}),
		ctx:       ctx,
		errChan:   make(chan error),
	}
	return uc.initialize()
}

// ListenUDP creates a new UDP server.
func listenUDP(network string, laddr *net.UDPAddr) (*uDPConn, error) {
	return listenUDPContext(context.Background(), network, laddr)
}

// ListenUDPContext creates a UDP listener that can be canceled with the provided context.
func listenUDPContext(ctx context.Context, network string, laddr *net.UDPAddr) (*uDPConn, error) {
	conn, err := net.ListenUDP(network, laddr)
	if err != nil {
		return nil, err
	}
	uc := &uDPConn{
		udpConn:   conn,
		closeChan: make(chan struct{}),
		ctx:       ctx,
		errChan:   make(chan error),
	}
	return uc.initialize()
}

func (conn *uDPConn) StopListening() error {
	return conn.Close()
}

// Close closes the udp conn.
func (conn *uDPConn) Close() error {
	close(conn.closeChan)
	return conn.udpConn.Close()
}

// CloseChan returns a channel that is closed when the connection gets closed.
func (conn *uDPConn) CloseChan() <-chan struct{} {
	return conn.closeChan
}

// Context returns the context associated with the conn.
func (conn *uDPConn) Context() context.Context {
	return conn.ctx
}

// initialize initializes a UDP connection.
func (conn *uDPConn) initialize() (*uDPConn, error) {
	if err := conn.udpConn.SetWriteBuffer(bufSize); err != nil {
		return nil, fmt.Errorf("setting write buffer size: %v", err)
	}
	return conn, nil

}

// read reads bytes and returns the net.Addr of the sender.
func (conn *uDPConn) read(data []byte) (int, net.Addr, error) {
	return conn.ReadFromUDP(data)
}

/*
// Send sends an OSC message over UDP.
func (conn *UDPConn) Send(p Packet) error {
	//	fmt.Printf("sending packet: %s\n", string(p.Bytes()))
	//_, err := conn.Write(p.Bytes()[:8])
	_, err := conn.Write(p.bytes())
	return err
}

var _ Sender = &UDPConn{}

// SendTo sends a packet to the given address.
func (conn *UDPConn) SendTo(addr net.Addr, p Packet) error {
	_, err := conn.WriteTo(p.bytes(), addr)
	return err
}
*/

// Serve starts dispatching OSC.
// Any errors returned from a dispatched method will be returned.
// Note that this means that errors returned from a dispatcher method will kill your server.
// If context.Canceled or context.DeadlineExceeded are encountered they will be returned directly.
func (conn *uDPConn) StartListening(recv Handler) error {
	return serve(conn, &dispatcherr{recv})
}

// SetContext sets the context associated with the conn.
func (conn *uDPConn) SetContext(ctx context.Context) {
	conn.ctx = ctx
}
