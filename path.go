package osc

import (
	"fmt"
	"io"
	"regexp"
	"strings"
)

// Path is an OSC path/route
type Path string

// String returns the string representation of the path
func (p Path) String() string {
	return string(p)
}

// HasPrefix returns wether path has the given prefix
func (p Path) HasPrefix(prefix string) bool {
	return strings.HasPrefix(string(p), prefix)
}

// HasSuffix returns wether path has the given suffix
func (p Path) HasSuffix(suffix string) bool {
	return strings.HasSuffix(string(p), suffix)
}

// Matches return wether path matches to the given regex
func (p Path) Matches(reg *regexp.Regexp) bool {
	return reg.MatchString(string(p))
}

// Scan scans values from the path based on format
func (p Path) Scan(format string, vals ...interface{}) (int, error) {
	return fmt.Sscanf(string(p), format, vals...)
}

// Set sets the values in the path and returns a new path where the values are set.
func (p Path) Set(vals ...interface{}) Path {
	return Path(fmt.Sprintf(string(p), vals...))
}

// WriteTo writes a message of the path and given values to the given writer.
func (p Path) WriteTo(wr io.Writer, vals ...interface{}) error {
	m := msg(string(p))

	for i, val := range vals {
		switch v := val.(type) {
		case float64:
			m.f(float32(v))
		case float32:
			m.f(v)
		case uint8:
			m.i(int32(v))
		case uint16:
			m.i(int32(v))
		case uint64:
			m.i(int32(v))
		case uint32:
			m.i(int32(v))
		case int8:
			m.i(int32(v))
		case int16:
			m.i(int32(v))
		case int64:
			m.i(int32(v))
		case int:
			m.i(int32(v))
		case uint:
			m.i(int32(v))
		case int32:
			m.i(v)
		case string:
			m.s(v)
		case []byte:
			m.b(v)
		case bool:
			if v {
				m.true()
			} else {
				m.false()
			}
		default:
			return fmt.Errorf("unsupported type: %T in value %d", val, i)
		}
	}

	_, err := m.WriteTo(wr)
	return err
}
