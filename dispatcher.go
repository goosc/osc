package osc

import (
	"fmt"
	"io"
	"strings"
	"time"
)

// Dispatcher dispatches OSC packets.
type dispatcherr struct {
	recv Handler
}

// Dispatch invokes an OSC bundle's messages.
func (d *dispatcherr) dispatchh(b Bundle) error {
	var (
		now = time.Now()
		tt  = b.timetag.Time()
	)
	if tt.Before(now) {
		return d.immediately(b)
	}
	<-time.After(tt.Sub(now))
	return d.immediately(b)
}

// immediately invokes an OSC bundle immediately.
func (d *dispatcherr) immediately(b Bundle) error {
	for _, p := range b.packets {
		errs := []string{}
		if err := d.invoke(p); err != nil {
			errs = append(errs, err.Error())
		}
		if len(errs) > 0 {
			return fmt.Errorf(strings.Join(errs, " and "))
		}
	}
	return nil
}

// invoke invokes an OSC packet, which could be a message or a bundle of messages.
func (d *dispatcherr) invoke(p io.WriterTo) error {
	switch x := p.(type) {
	case Message:
		return d.invoke2(x)
	case Bundle:
		return d.immediately(x)
	default:
		return fmt.Errorf("unsupported type for dispatcher: %T", p)
	}
}

// Invoke invokes an OSC message.
func (d *dispatcherr) invoke2(msg Message) error {
	p := Path(msg.path)
	if d.recv.Matches(p) {
		d.recv.Handle(p, msg.values...)
	}
	return nil
}
