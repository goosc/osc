package osc

import (
	"bytes"
	"fmt"
	"io"
)

type Message struct {
	path   string
	values []interface{}
}

func (m Message) Path() Path {
	return Path(m.path)
}

func (m Message) Values() []interface{} {
	return m.values
}

// S adds a string to the message
func (m *Message) s(s string) *Message {
	m.values = append(m.values, s)
	return m
}

// I adds an integer to the message
func (m *Message) i(i int32) *Message {
	m.values = append(m.values, i)
	return m
}

// F adds a float to the message
func (m *Message) f(f float32) *Message {
	m.values = append(m.values, f)
	return m
}

// True adds true to the message
func (m *Message) true() *Message {
	m.values = append(m.values, true)
	return m
}

// False adds false to the message
func (m *Message) false() *Message {
	m.values = append(m.values, false)
	return m
}

// B adds a blob to the message
func (m *Message) b(b []byte) *Message {
	m.values = append(m.values, b)
	return m
}

// Msg returns a message for the given path
func msg(path string) *Message {
	return &Message{path: path}
}

// MsgF returns a message for the given path and path values
func msgF(path string, pathVals ...interface{}) *Message {
	return &Message{path: fmt.Sprintf(path, pathVals...)}
}

// Typetags returns a padded byte slice of the message's type tags.
func (m Message) typetags() (b []byte) {
	b = make([]byte, len(m.values)+1)
	b[0] = ',' /* type prefix */
	for i, v := range m.values {
		b[i+1] = typeTag(v)
	}
	return pad(append(b, 0))
}

// Bytes returns the contents of the message as a slice of bytes.
func (m Message) bytes() []byte {
	b := [][]byte{
		stringToBytes(m.path),
		m.typetags(),
	}
	for _, a := range m.values {
		b = append(b, argToBytes(a))
	}
	return bytes.Join(b, []byte{})
}

// parseMessage parses an OSC message from a slice of bytes.
func _parseMessage(data []byte) (Message, error) {
	address, idx := readString(data)
	msg := Message{
		path: address,
		//Sender:  sender,
	}
	data = data[idx:]
	typetags, idx := readString(data)
	data = data[idx:]

	// Read all arguments.
	args, err := readArguments([]byte(typetags), data)
	if err != nil {
		return Message{}, err
	}
	msg.values = args

	return msg, nil
}

// WriteTo writes the Message to an io.Writer.
func (m Message) WriteTo(w io.Writer) (int64, error) {
	n, err := w.Write(m.bytes())
	return int64(n), err
}
