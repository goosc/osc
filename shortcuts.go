package osc

import (
	"fmt"
	"io"
)

// SI shortcut for a Message with a string and an int argument
type SI string

func (si SI) Set(vals ...interface{}) SI {
	return SI(fmt.Sprintf(string(si), vals...))
}

func (si SI) WriteTo(wr io.Writer, s string, i int32) error {
	return Path(string(si)).WriteTo(wr, s, i)
}

// IS shortcut for a Message with an int and a string argument
type IS string

func (si IS) Set(vals ...interface{}) IS {
	return IS(fmt.Sprintf(string(si), vals...))
}

func (si IS) WriteTo(wr io.Writer, i int32, s string) error {
	return Path(string(si)).WriteTo(wr, i, s)
}

// FS shortcut for a Message with a float and a string argument
type FS string

func (i FS) Set(vals ...interface{}) FS {
	return FS(fmt.Sprintf(string(i), vals...))
}

func (i FS) WriteTo(wr io.Writer, f float32, s string) error {
	return Path(string(i)).WriteTo(wr, f, s)
}

// SF shortcut for a Message with a string and a float argument
type SF string

func (i SF) Set(vals ...interface{}) SF {
	return SF(fmt.Sprintf(string(i), vals...))
}

func (i SF) WriteTo(wr io.Writer, s string, f float32) error {
	return Path(string(i)).WriteTo(wr, s, f)
}

// IF shortcut for a Message with an int and a float argument
type IF string

func (si IF) Set(vals ...interface{}) IF {
	return IF(fmt.Sprintf(string(si), vals...))
}

func (si IF) WriteTo(wr io.Writer, i int32, f float32) error {
	return Path(string(si)).WriteTo(wr, i, f)
}

// FI shortcut for a Message with a float and an int argument
type FI string

func (si FI) Set(vals ...interface{}) FI {
	return FI(fmt.Sprintf(string(si), vals...))
}

func (si FI) WriteTo(wr io.Writer, f float32, i int32) error {
	return Path(string(si)).WriteTo(wr, f, i)
}

// S shortcut for a Message with one string argument
type S string

func (i S) Set(vals ...interface{}) S {
	return S(fmt.Sprintf(string(i), vals...))
}

func (i S) WriteTo(wr io.Writer, v string) error {
	return Path(string(i)).WriteTo(wr, v)
}

// SS shortcut for a Message with two string arguments
type SS string

func (i SS) Set(vals ...interface{}) SS {
	return SS(fmt.Sprintf(string(i), vals...))
}

func (i SS) WriteTo(wr io.Writer, s1, s2 string) error {
	return Path(string(i)).WriteTo(wr, s1, s2)
}

// SSS shortcut for a Message with three string arguments
type SSS string

func (i SSS) Set(vals ...interface{}) SSS {
	return SSS(fmt.Sprintf(string(i), vals...))
}

func (i SSS) WriteTo(wr io.Writer, s1, s2, s3 string) error {
	return Path(string(i)).WriteTo(wr, s1, s2, s3)
}

// I shortcut for a Message with one int argument
type I string

func (i I) Set(vals ...interface{}) I {
	return I(fmt.Sprintf(string(i), vals...))
}

func (i I) WriteTo(wr io.Writer, v int32) error {
	return Path(string(i)).WriteTo(wr, v)
}

// II shortcut for a Message with two int arguments
type II string

func (i II) Set(vals ...interface{}) II {
	return II(fmt.Sprintf(string(i), vals...))
}

func (i II) WriteTo(wr io.Writer, i1, i2 int32) error {
	return Path(string(i)).WriteTo(wr, i1, i2)
}

// III shortcut for a Message with three int arguments
type III string

func (i III) Set(vals ...interface{}) III {
	return III(fmt.Sprintf(string(i), vals...))
}

func (i III) WriteTo(wr io.Writer, i1, i2, i3 int32) error {
	return Path(string(i)).WriteTo(wr, i1, i2, i3)
}

// F shortcut for a Message with one float argument
type F string

func (i F) Set(vals ...interface{}) F {
	return F(fmt.Sprintf(string(i), vals...))
}

func (i F) WriteTo(wr io.Writer, v float32) error {
	return Path(string(i)).WriteTo(wr, v)
}

// FF shortcut for a Message with two float arguments
type FF string

func (i FF) Set(vals ...interface{}) FF {
	return FF(fmt.Sprintf(string(i), vals...))
}

func (i FF) WriteTo(wr io.Writer, f1, f2 float32) error {
	return Path(string(i)).WriteTo(wr, f1, f2)
}

// FFF shortcut for a Message with three float arguments
type FFF string

func (i FFF) Set(vals ...interface{}) FFF {
	return FFF(fmt.Sprintf(string(i), vals...))
}

func (i FFF) WriteTo(wr io.Writer, f1, f2, f3 float32) error {
	return Path(string(i)).WriteTo(wr, f1, f2, f3)
}

// B shortcut for a Message with one blob argument
type B string

func (i B) Set(vals ...interface{}) B {
	return B(fmt.Sprintf(string(i), vals...))
}

func (i B) WriteTo(wr io.Writer, v []byte) error {
	return Path(string(i)).WriteTo(wr, v)
}

type Toggle string

func (i Toggle) Set(vals ...interface{}) Toggle {
	return Toggle(fmt.Sprintf(string(i), vals...))
}

func (i Toggle) WriteTo(wr io.Writer) error {
	return Path(string(i)).WriteTo(wr)
}

type NoArg string

func (i NoArg) Set(vals ...interface{}) NoArg {
	return NoArg(fmt.Sprintf(string(i), vals...))
}

func (i NoArg) WriteTo(wr io.Writer) error {
	return Path(string(i)).WriteTo(wr)
}

type True string

func (i True) Set(vals ...interface{}) True {
	return True(fmt.Sprintf(string(i), vals...))
}

func (i True) WriteTo(wr io.Writer) error {
	return Path(string(i)).WriteTo(wr, true)
}

type False string

func (i False) Set(vals ...interface{}) False {
	return False(fmt.Sprintf(string(i), vals...))
}

func (i False) WriteTo(wr io.Writer) error {
	return Path(string(i)).WriteTo(wr, false)
}
