package osc

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io"
)

// pad pads a slice of bytes with null bytes so that it's length is a multiple of 4.
func pad(b []byte) []byte {
	for i := len(b); (i % 4) != 0; i++ {
		b = append(b, 0)
	}
	return b
}

// ReadString reads a string from a byte slice.
// If the byte slice does not have any null bytes,
// then one is appended to the end.
// If the length of the byte slice is not a multiple of 4
// we append as many null bytes as we need to make this true
// before converting to a string.
// What this means is that the second return value, which is
// the number of bytes that are consumed to create the string is
// always a multiple of 4.
// We also strip off any trailing null bytes in the returned string.
func readString(data []byte) (string, int64) {
	if len(data) == 0 {
		return "", 0
	}
	nullidx := bytes.IndexByte(data, 0)
	if nullidx == -1 {
		data = append(data, 0)
		nullidx = len(data) - 1
	}
	data = pad(data[:nullidx+1])
	return string(bytes.TrimRight(data, "\x00")), int64(len(data))
}

// readBlob reads a blob of the given length from the given slice of bytes.
func readBlob(length int32, data []byte) ([]byte, int64) {
	l := length
	if length > int32(len(data)) {
		l = int32(len(data))
	}

	var idx int32
	for idx = l; (idx % 4) != 0; idx++ {
		if idx >= int32(len(data)) {
			data = append(data, 0)
		}
	}
	return data[:idx], int64(idx)
}

func stringToBytes(s string) []byte {
	if len(s) == 0 {
		return []byte{}
	}
	return pad(append([]byte(s), 0))
}

func intToBytes(i int32) []byte {
	return []byte{
		byte(i >> 24),
		byte(i >> 16),
		byte(i >> 8),
		byte(i),
	}
}

// Bytes converts the arg to a byte slice suitable for adding to the binary representation of an OSC message.
func floatToBytes(f float32) []byte {
	var (
		buf = &bytes.Buffer{}
		_   = binary.Write(buf, byteOrder, f) // Never fails
	)
	return buf.Bytes()
}

// Bytes converts the arg to a byte slice suitable for adding to the binary representation of an OSC message.
func boolToBytes() []byte {
	return []byte{}
}

func blobToBytes(b []byte) []byte {
	return pad(bytes.Join([][]byte{
		intToBytes(int32(len(b))),
		b,
	}, []byte{}))
}

func argToBytes(arg interface{}) []byte {
	switch v := arg.(type) {
	case int32:
		return intToBytes(v)
	case float32:
		return floatToBytes(v)
	case bool:
		return boolToBytes()
	case string:
		return stringToBytes(v)
	case []byte:
		return blobToBytes(v)
	}
	panic(fmt.Sprintf("unknown type %T (%#v) in argument", arg))
}

// readArguments reads all arguments from the reader and adds it to the OSC message.
func readArguments(typetags, data []byte) ([]interface{}, error) {
	args := []interface{}{}

	// Strip off the prefix.
	if len(typetags) > 0 && typetags[0] == ',' /* type prefix */ {
		typetags = typetags[1:]
	}

	for i, tt := range typetags {
		arg, idx, err := readArgument(tt, data)
		if err != nil {
			return nil, fmt.Errorf("read argument %d: %v", i, err)
		}
		args = append(args, arg)
		data = data[idx:]
	}
	return args, nil
}

// ReadArgument parses an OSC message argument given a type tag and some data.
func readArgument(typetag byte, data []byte) (interface{}, int64, error) {
	switch typetag {
	case 'i':
		var i int32
		if err := binary.Read(bytes.NewReader(data), byteOrder, &i); err != nil {
			return nil, 0, fmt.Errorf("read int argument: %v", err)
		}
		return i, 4, nil
	case 'f':
		var f float32
		if err := binary.Read(bytes.NewReader(data), byteOrder, &f); err != nil {
			return nil, 0, fmt.Errorf("read float argument: %v", err)
		}
		return f, 4, nil
	case 'T':
		return true, 0, nil
	case 'F':
		return false, 0, nil
	case 's':
		s, idx := readString(data)
		return s, idx, nil
	case 'b':
		//return ReadBlobFrom(data)
		var length int32
		if err := binary.Read(bytes.NewReader(data), byteOrder, &length); err != nil {
			return nil, 0, fmt.Errorf("read blob argument: %v", err)
		}
		b, bl := readBlob(length, data[4:])
		return b, bl + 4, nil
	}
	return nil, 0, fmt.Errorf("invalid typetag %q", string(typetag))
}

func writeArgument(w io.Writer, arg interface{}) (int64, error) {
	switch v := arg.(type) {
	case int32:
		written, err := fmt.Fprintf(w, "%d", v)
		return int64(written), err
	case float32:
		written, err := fmt.Fprintf(w, "%f", v)
		return int64(written), err
	case bool:
		written, err := fmt.Fprintf(w, "%t", v)
		return int64(written), err
	case string:
		written, err := fmt.Fprintf(w, "%s", v)
		return int64(written), err
	case []byte:
		written, err := w.Write(v)
		return int64(written), err
		//			panic(fmt.Sprintf("unknown type %T (%#v) in argument %v", a, a, i))
	}
	return 0, fmt.Errorf("unknown type %T (%#v) in argument", arg, arg)
}

func typeTag(a interface{}) byte {
	switch v := a.(type) {
	case int32:
		return 'i'
	case float32:
		return 'f'
	case bool:
		if v {
			return 'T'
		} else {
			return 'F'
		}
	case string:
		return 's'
	case []byte:
		return 'b'
	default:
		panic(fmt.Sprintf("unknown type %T (%#v) in argument", a, a))
	}
}
