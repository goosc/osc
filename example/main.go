package main

import (
	"fmt"

	"gitlab.com/goosc/osc"
)

const (
	click     osc.Path = "/click"
	play      osc.Path = "/play"
	trackSolo osc.Path = "/track/%d/solo"
)

type receiver struct{}

func (r receiver) Matches(p osc.Path) bool { return p != "/time/str" }

func (r receiver) Handle(p osc.Path, values ...interface{}) {
	fmt.Printf("got: %#v (%v)\n", p, values)
	if p.HasPrefix("/track/") {
		var track int
		p.Scan("/track/%d", &track)
		fmt.Printf("track is %d\n", track)
	}
}

func main() {
	var _c chan bool
	listener, err := osc.UDPListener("127.0.0.1:9000")
	errPanic(err)

	defer listener.StopListening()

	go listener.StartListening(receiver{})

	wr, err := osc.UDPWriter("127.0.0.1:8000")
	errPanic(err)
	defer wr.Close()

	click.WriteTo(wr, 1)
	play.WriteTo(wr, 1)
	trackSolo.Set(1).WriteTo(wr, 1)
	<-_c
}

func errPanic(err error) {
	if err != nil {
		panic(err.Error())
	}
}
