package osc

import "encoding/binary"

// Handler handles incoming OSC messages
type Handler interface {

	// Matches returns wether the given path should be handled by the handler
	Matches(path Path) bool

	// Handle handles the incoming message
	Handle(path Path, values ...interface{})
}

type Listener interface {
	StartListening(Handler) error
	StopListening() error
}

var byteOrder = binary.BigEndian
